#include "player.h"
#include "defs.h"
#include "sdk/lib/blitter/blitter.h" // low level items ?? for anim

#include "game.h"
#include "gamepad.h"

#define IDLE_STATE items16_send_spr

Player::Player(void)
{
	dir(Dir::down);
	state(IDLE_STATE); // allows removing it later
	idle();
	blitter_insert(this,0,1024,0);

	sword.state(IDLE_STATE); // whatever, will be removed
	blitter_insert(&sword,0,1024,0);
	z=-5;
}
 
void Player::walk (Dir dir)
{
	this->dir(dir);
	update= update_walk;
	state_dir (hero_walk_dn_spr);
}

void Player::idle()
{
	update= update_idle;
	state_dir (hero_idle_dn_spr);
}

void Player::update_idle (Object *self, Collision c)
{
	auto &p = static_cast<Player&>(*self);
	p.anim_frame();

	auto d = Gamepad::dir();

	if (d != Dir::none) {
		p.walk(d);
	}

	if (Gamepad::attack()) { p.sword_attack(); }
	p.update_sword();
}

void Player::sword_attack()
{
	if (game.status.sword == sword_none)  return; // no sword yet
	if (sword.state() != IDLE_STATE) return; // already attacking

	// init object sword
	static const FilesEnum base_state = game.status.sword == sword_stick ? sword_stick_dn_spr : sword_sword_dn_spr;
	sword.state(base_state + static_cast<int>(dir()));

	sword.x = x;
	sword.y = y;

	// player attack state
	state_dir(hero_attack_dn_spr);
}

void Player::update_sword()
{
	if (sword.state() == IDLE_STATE) return;

	// follow player
	sword.x = x;
	sword.y = y;

	if ( vga_frame%8 ==0) sword.fr++;

	if (sword.fr >= sword.def()->frames) {
		sword.state(IDLE_STATE);
		sword.y = 1024;
		idle();
	}
}

// facing direction still pressed ?
bool Player::still_pressed()
{
	switch (dir()) {
		case Dir::up:    return Gamepad::up();
		case Dir::down:  return Gamepad::down();
		case Dir::left:  return Gamepad::left();
		case Dir::right: return Gamepad::right();
		case Dir::none : return false;
	}
	return false;
}

void Player::invincible() {
	if (invincibility_frames) {
		if (invincibility_frames/8%2)
			hide();
		else 
			show();
		invincibility_frames--;
	}
}

void Player::update_walk (Object *self, Collision col)
{
	auto &p = static_cast<Player&>(*self);
	p.anim_frame(); // loop
	
	if (col) self->block(col);

	int prev_x=p.x;
	int prev_y=p.y;

	bool chgdir = !p.still_pressed();

	// update player from controls
	if ( Gamepad::left() ) {
		if (chgdir) p.walk(Dir::left);
		p.x -= 1;
	} else if ( Gamepad::right() ) {
		if (chgdir) p.walk(Dir::right);
		p.x += 1;
	}
	
	// independent check
	if ( Gamepad::up() ) {
		if (chgdir) p.walk(Dir::up);
		p.y -= 1;
	} else if ( Gamepad::down() ) {
		if (chgdir) p.walk(Dir::down);
		p.y += 1;
	}

	// set idle if no direction pressed
	if ( prev_y==p.y && prev_x==p.x ) {
		p.idle();
	}

	if (Gamepad::attack()) { p.sword_attack(); }
	p.update_sword();

}

// play die animation and return 
// fixme use anim_update
void Player::die_anim()
{
	state(hero_die_spr);
	//sword.hide();

	// play animation once
	while (fr < def()->frames) {
		wait_vsync(8);
		fr += 1;
	}
}

void Player::take(int item_id) // file_id in fact
{
	switch (item_id) {
		case items16_letter_spr : 
			take_anim(item_id); 
			game.window.dialog(0,_("Hello, it's dad.\n"
			 "Your mother is asleep, and I went getting some mushrooms in the forest.\n"
			 "Sleep well, and don't try to sneak outside !"
			 ),"OK\nAgain");
			break;

		case champi_16x16_idle_spr : 
			if (game.status.mushrooms==0)
				take_anim(item_id); 
			game.status.mushrooms++;
			game.window.update_hud();
			break;
	}

	// game.status.items |= 1<<item_id;
}

void Player::hurt(int damage) 
{
	if (!invincibility_frames) {
		message ("ouch ! \n");
		// TODO play sound hurt
		if (game.status.life>damage) {
			game.status.life-=damage; 
			game.window.update_hud();
			invincibility_frames = 60;
		} else {
			die_anim();
			game.status.life = 8; // fixme 
			game.window.update_hud();
			// game over ?
		}
	}
}

void Player::take_anim(int file_id)
{

	int old_state = state(); // save player state
	state(hero_receive_spr);

	// display item as a raw sprite
	object spr;
	sprite3_load (&spr, game.files.load_file(file_id));
	blitter_insert(&spr, x+15,y+2,-1 ); // front of player

	for (int i=0;i<10;i++) {
		spr.y-=1;
		wait_vsync(6);
	}
	wait_vsync(20); // little pause

	blitter_remove(&spr); 
	game.files.free_file((void*)spr.a);

	state(old_state);
}

void Player::pull(Object *o)
{
	message("Start pulling object\n");
	hold = o;
	state_dir (hero_pull_dn_spr);
	update = update_pull;
}

void Player::update_pull (Object *self, Collision col)
{
	auto &p = static_cast<Player&>(*self);
	p.anim_frame();

	if (col) {
		message("collide while pulling\n");
		p.block(col); // avoid collision with hold object !
	}

	// stop pulling
	if (!GAMEPAD_PRESSED(0,B)) {
		p.hold = 0;
		p.idle();
		return;
	}

	switch (p.dir()) {
		case Dir::left : 
			if (Gamepad::right()) {
				p.hold->x += 1;
				p.x += 1;
			}
			break;

		case Dir::right : 
			if (Gamepad::left())  {
				p.x -= 1;
				p.hold->x -= 1;
			}
			break;

		case Dir::up : 
			if (Gamepad::down()) {
				p.y += 1;
				p.hold->y += 1;
			} 
			break;

		case Dir::down : 
			if (Gamepad::up()) {
				p.y -= 1;
				p.hold->y -= 1;
			}   
			break;

		default: 
			break;					
	}

	// update pulled object position

}

void Player::fall_anim()
{
	state(hero_falling_spr);

	wait_vsync(10);
	while (!anim_frame()) {
		wait_vsync(1);
	}
}
