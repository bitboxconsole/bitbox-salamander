/* behaviours of start, underground, up to forest */
#include "miniz.h"
#include "defs.h"
#include "game.h"

const char *window_msgs[4] = {
	_("Such a vast world and I'm here !"),
	_("I can't sleep ! I want to go out !"),
	_("The moon is so nice"),
	_("Why can't I go see what's outside ?"),
};

void update_start_window (Object *, Collision col) 
{
	static int dlg=3;
	// if touch, say sth, collide
	if (col) {
		game.window.dialog(0,window_msgs[dlg++%4],"ok\nSigh ..."); // message
		message ("start dlg %d\n",dlg);
	}
}

void update_old_pnj(Object *self, Collision col)
{
	if (!col) return;

	if (!game.status.start_laby_talked_old) {
		game.window.dialog (faces_old_pnj_spr,_("It's dangerous to go alone. \nTake this."),0);
		game.room.player.take_anim(items16_wine_spr); 
		game.window.dialog (faces_player_spr, _("Well, ... thank you"),"...");
		game.window.dialog (faces_old_pnj_spr,_("What ?"),0);
		game.window.dialog (faces_player_spr, _("It's a bottle of wine ! \nDon't you think I'm too young for this ?"),0);
		game.window.dialog (faces_old_pnj_spr,_("Ah ! Of course. Give it back to me"),0);
		// window_draw_hud();
		game.status.start_laby_talked_old=1;
	} else if (game.status.sword==0) {
		game.window.dialog (faces_old_pnj_spr,_("It's dangerous to go alone. \nTake this."),0);
		game.room.player.take_anim(items16_sword_stick_spr); 
		game.window.dialog (faces_player_spr, _("Well, ... thank you"),"...");
		game.window.dialog (faces_old_pnj_spr,_("What, *again* ?"),0);
		game.window.dialog (faces_player_spr, _("It's just a wood stick !"),0);
		int ans =  game.window.dialog (faces_old_pnj_spr,_("Spoiled child !\n Do you want it or not ?"),_("No .. \nYes !"));
		if (ans) {
			// say something for usage
			game.status.sword=sword_stick;
		}		
	} else {
		game.window.dialog (faces_old_pnj_spr,_("It's dangerous to go alone.\nBut you have my stick now.\nLeave me alone."),0);
		game.window.dialog (faces_player_spr, _("But won't it be dangerous to be alone ?"),0);
		game.window.dialog (faces_old_pnj_spr,_("uuh .. Anyway, let me heal you."),0);
		game.status.life = game.status.life_max;
	}
	game.window.update_hud();
}

template <typename T>
inline static int sign(T a) { return (a>0)-(a<0); }


// dialogs for town
static void guard_dialog() 
{
	game.window.dialog (faces_guard_spr, 
		_("Hi boy, what are you doing ?\n")
		_("I thought your father never let you go out alone ?"),

		_("...")
		);

	const int ans = game.window.dialog (faces_player_spr, _("Well, ..."),
		_("Mind your own business !\n" )
		_("My father is not well !\n")
		_("I'm waiting for him !")
	);

	switch (ans) {
		case 0 : 
			game.window.dialog (faces_guard_angry_spr,
				_("Young boy, I'm a friend of your father,\nand I don't like the way you talk to me. "),
				0);

			game.window.dialog (faces_guard_angry_spr,
				_("And the fact that you're alone at this late hour is my business indeed. ")
				_("Come here, I'll bring you home."),
				0);
			break;
		case 1 : 
			game.window.dialog (faces_guard_spr, 
				_("Oh, your father is in bad shape ?\n")
				_("Well, it's good that you told me.\n"),
				0);

			game.window.dialog (faces_guard_spr, 
  			    _("I'll make sure to check in on him.\n")
				_("Let's go back inside."),
				0);

			game.window.dialog (faces_player_spr, _("Oh, well ..."),0);
			break;
		case 2 : 
		default: 
			game.window.dialog (faces_guard_spr,
				_("Well I haven't seen him around.\n")
				_("I was returning from the Happy Boar tavern."),
				0);

			game.window.dialog (faces_guard_spr, _("Let's have you wait inside in any case..."),0);
			game.window.dialog (faces_player_spr, "...",0);
	}
}


void update_town_guard(Object *self, Collision col)
{
	// go to player
	self->x += sign( game.room.player.x - self->x);
	self->y += sign( game.room.player.y - self->y);

	if (col) {
		if (game.status.town_nuit_guard_talked<3) {
			game.status.town_nuit_guard_talked++;
			guard_dialog();
		} else {
			game.window.dialog (faces_guard_angry_spr, _("Get back to sleep immediately ! "),0);
		}

		game.room.fadeOut();
		game.room.unload();
		game.room.load(room_start,0);
	}
}