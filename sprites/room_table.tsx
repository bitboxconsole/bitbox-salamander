<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="room_table" tilewidth="39" tileheight="33" tilecount="1" columns="1">
 <image source="../sprites/room_table.png" trans="ff00da" width="39" height="33"/>
 <tile id="0" type="table">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="39" height="29"/>
  </objectgroup>
 </tile>
</tileset>
