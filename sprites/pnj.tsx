<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="pnj" tilewidth="16" tileheight="22" tilecount="27" columns="9">
 <image source="pnj.png" trans="ff00da" width="144" height="80"/>
 <tile id="0" type="hermit">
  <properties>
   <property name="update" value="old_pnj"/>
  </properties>
  <animation>
   <frame tileid="0" duration="133"/>
   <frame tileid="0" duration="133"/>
   <frame tileid="0" duration="133"/>
   <frame tileid="0" duration="133"/>
   <frame tileid="1" duration="133"/>
   <frame tileid="1" duration="100"/>
  </animation>
 </tile>
 <tile id="2" type="dad_wait">
  <properties>
   <property name="update" value="dad"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="16" height="14"/>
  </objectgroup>
  <animation>
   <frame tileid="2" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="3" duration="133"/>
   <frame tileid="3" duration="133"/>
  </animation>
 </tile>
 <tile id="3" type="dad_pick">
  <objectgroup draworder="index">
   <object id="1" x="0" y="9" width="16" height="13"/>
  </objectgroup>
  <animation>
   <frame tileid="3" duration="133"/>
   <frame tileid="4" duration="133"/>
   <frame tileid="5" duration="133"/>
   <frame tileid="6" duration="133"/>
   <frame tileid="7" duration="133"/>
   <frame tileid="6" duration="133"/>
   <frame tileid="5" duration="133"/>
   <frame tileid="4" duration="133"/>
  </animation>
 </tile>
 <tile id="9" type="dad_sit">
  <objectgroup draworder="index">
   <object id="1" x="5" y="8" width="8" height="13"/>
  </objectgroup>
  <animation>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="9" duration="133"/>
   <frame tileid="10" duration="133"/>
  </animation>
 </tile>
 <tile id="11" type="dad_walk_l">
  <objectgroup draworder="index">
   <object id="1" x="5" y="9" width="7" height="12"/>
  </objectgroup>
  <animation>
   <frame tileid="11" duration="133"/>
   <frame tileid="12" duration="133"/>
  </animation>
 </tile>
 <tile id="13" type="dad_walk_r">
  <objectgroup draworder="index">
   <object id="1" x="5" y="9" width="7" height="12"/>
  </objectgroup>
  <animation>
   <frame tileid="13" duration="133"/>
   <frame tileid="14" duration="133"/>
  </animation>
 </tile>
</tileset>
