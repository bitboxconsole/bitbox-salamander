<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="house_border" tilewidth="8" tileheight="57" tilecount="2" columns="2">
 <image source="house_border.png" trans="ff00da" width="16" height="57"/>
 <tile id="0" type="left"/>
 <tile id="1" type="right"/>
</tileset>
