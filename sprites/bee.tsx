<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="bee" tilewidth="10" tileheight="10" tilecount="8" columns="2">
 <grid orientation="orthogonal" width="8" height="8"/>
 <image source="bee_8x8.png" trans="d92ccf" width="20" height="40"/>
 <tile id="0">
  <properties>
   <property name="update" value="bee"/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
  </animation>
 </tile>
 <tile id="2">
  <properties>
   <property name="update" value="bee"/>
  </properties>
 </tile>
 <tile id="4" type="right">
  <properties>
   <property name="update" value="bee"/>
  </properties>
  <animation>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
  </animation>
 </tile>
 <tile id="6" type="left">
  <properties>
   <property name="update" value="bee"/>
  </properties>
  <animation>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
  </animation>
 </tile>
</tileset>
