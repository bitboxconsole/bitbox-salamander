<?xml version="1.0" encoding="UTF-8"?>
<tileset name="vase" tilewidth="16" tileheight="16" tilecount="16" columns="4">
 <image source="items16.png" trans="ff00da" width="64" height="64"/>
 <tile id="4" type="idle">
  <properties>
   <property name="empty" type="int" value="0"/>
   <property name="heart" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="5" type="break">
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="6" duration="100"/>
  </animation>
 </tile>
</tileset>
