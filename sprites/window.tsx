<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="window_11x28" tilewidth="11" tileheight="28" tilecount="2" columns="2">
 <properties>
  <property name="collide" value="start_window"/>
 </properties>
 <image source="window_11x28.png" trans="ff00da" width="22" height="28"/>
 <tile id="0" type="left">
  <properties>
   <property name="update" value="start_window"/>
  </properties>
 </tile>
 <tile id="1" type="right">
  <properties>
   <property name="update" value="start_window"/>
  </properties>
 </tile>
</tileset>
