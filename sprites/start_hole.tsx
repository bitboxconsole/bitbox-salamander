<?xml version="1.0" encoding="UTF-8"?>
<tileset name="start_hole" tilewidth="32" tileheight="23" tilecount="1" columns="1">
 <image source="start_hole_32x23.png" trans="ff00da" width="32" height="23"/>
 <tile id="0" type="hole">
  <properties>
   <property name="update" value="sendto"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="6" y="5" width="19" height="13"/>
  </objectgroup>
 </tile>
</tileset>
