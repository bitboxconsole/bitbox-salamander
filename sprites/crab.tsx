<?xml version="1.0" encoding="UTF-8"?>
<tileset name="crab" tilewidth="32" tileheight="32" tilecount="80" columns="8">
 <properties>
  <property name="class" value="Crab"/>
 </properties>
 <image source="crab.png" width="256" height="320"/>
 <tile id="0" type="death">
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="6" duration="100"/>
  </animation>
 </tile>
 <tile id="8" type="attack_r">
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="9" duration="100"/>
  </animation>
 </tile>
 <tile id="16" type="walk_down">
  <animation>
   <frame tileid="16" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="18" duration="100"/>
   <frame tileid="19" duration="100"/>
   <frame tileid="20" duration="100"/>
   <frame tileid="21" duration="100"/>
  </animation>
 </tile>
 <tile id="24" type="idle_r">
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="8" duration="100"/>
   <frame tileid="27" duration="100"/>
  </animation>
 </tile>
 <tile id="40" type="idle_up">
  <animation>
   <frame tileid="32" duration="100"/>
   <frame tileid="41" duration="100"/>
   <frame tileid="32" duration="100"/>
   <frame tileid="43" duration="100"/>
  </animation>
 </tile>
 <tile id="48" type="walk_left">
  <animation>
   <frame tileid="48" duration="100"/>
   <frame tileid="49" duration="100"/>
   <frame tileid="50" duration="100"/>
  </animation>
 </tile>
</tileset>
