<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="start_bed" tilewidth="38" tileheight="26" tilecount="1" columns="1">
 <image source="item-bed.png" width="38" height="26"/>
 <tile id="0" type="bed">
  <properties>
   <property name="update" value="canpull"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0.545455" y="4" width="37.2727" height="21.6364"/>
  </objectgroup>
 </tile>
</tileset>
