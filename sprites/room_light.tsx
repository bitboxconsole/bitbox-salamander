<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="room_light" tilewidth="10" tileheight="16" tilecount="4" columns="1">
 <image source="room_light_10x16.png" trans="ff00da" width="10" height="64"/>
 <tile id="0" type="light">
  <animation>
   <frame tileid="0" duration="133"/>
   <frame tileid="1" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="3" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="1" duration="133"/>
   <frame tileid="0" duration="133"/>
  </animation>
 </tile>
</tileset>
