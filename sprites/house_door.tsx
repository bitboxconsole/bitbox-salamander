<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="house_door" tilewidth="48" tileheight="45" tilecount="1" columns="1">
 <image source="house_door.png" trans="ff00da" width="48" height="45"/>
 <tile id="0" type="_">
  <objectgroup draworder="index">
   <object id="1" x="14" y="20" width="19" height="23"/>
  </objectgroup>
 </tile>
</tileset>
