<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="hero" tilewidth="32" tileheight="32" tilecount="85" columns="5">
 <image source="hero_cloth.png" trans="ff24ff" width="160" height="544"/>
 <tile id="0" type="attack_r">
  <objectgroup draworder="index" name="attack_r">
   <object id="1" x="12" y="8" width="11" height="19"/>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="4" duration="100"/>
  </animation>
 </tile>
 <tile id="5" type="walk_r">
  <objectgroup draworder="index">
   <object id="1" x="13" y="17" width="7" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="8" duration="100"/>
  </animation>
 </tile>
 <tile id="10" type="idle_r">
  <objectgroup draworder="index">
   <object id="1" x="12" y="18" width="8" height="7"/>
  </objectgroup>
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
  </animation>
 </tile>
 <tile id="13" type="pull_r">
  <objectgroup draworder="index">
   <object id="1" x="9" y="18" width="11" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
  </animation>
 </tile>
 <tile id="15" type="attack_up">
  <objectgroup draworder="index">
   <object id="1" x="10" y="16" width="10" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="15" duration="100"/>
   <frame tileid="16" duration="100"/>
   <frame tileid="19" duration="100"/>
  </animation>
 </tile>
 <tile id="20" type="walk_up">
  <objectgroup draworder="index">
   <object id="1" x="12" y="16.4659" width="8" height="8.5341"/>
  </objectgroup>
  <animation>
   <frame tileid="20" duration="250"/>
   <frame tileid="21" duration="250"/>
   <frame tileid="20" duration="250"/>
   <frame tileid="23" duration="250"/>
  </animation>
 </tile>
 <tile id="25" type="idle_up">
  <objectgroup draworder="index">
   <object id="1" x="13" y="19" width="6" height="7"/>
  </objectgroup>
  <animation>
   <frame tileid="26" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="20" duration="100"/>
  </animation>
 </tile>
 <tile id="27" type="pull_up">
  <objectgroup draworder="index">
   <object id="1" x="10" y="19" width="12" height="7"/>
  </objectgroup>
  <animation>
   <frame tileid="26" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="27" duration="100"/>
   <frame tileid="27" duration="100"/>
  </animation>
 </tile>
 <tile id="30" type="attack_dn">
  <objectgroup draworder="index">
   <object id="1" x="12" y="18" width="9" height="8"/>
  </objectgroup>
  <animation>
   <frame tileid="30" duration="100"/>
   <frame tileid="31" duration="100"/>
   <frame tileid="34" duration="100"/>
  </animation>
 </tile>
 <tile id="35" type="walk_dn">
  <objectgroup draworder="index">
   <object id="1" x="12" y="16.6932" width="8" height="8.92045"/>
  </objectgroup>
  <animation>
   <frame tileid="35" duration="250"/>
   <frame tileid="36" duration="250"/>
   <frame tileid="35" duration="250"/>
   <frame tileid="38" duration="250"/>
  </animation>
 </tile>
 <tile id="40" type="idle_dn">
  <objectgroup draworder="index">
   <object id="1" x="12" y="17" width="8" height="10"/>
  </objectgroup>
  <animation>
   <frame tileid="35" duration="133"/>
   <frame tileid="41" duration="133"/>
   <frame tileid="41" duration="133"/>
   <frame tileid="41" duration="133"/>
  </animation>
 </tile>
 <tile id="43" type="pull_dn">
  <objectgroup draworder="index">
   <object id="1" x="10" y="18" width="12" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="43" duration="100"/>
   <frame tileid="43" duration="100"/>
   <frame tileid="41" duration="100"/>
   <frame tileid="41" duration="100"/>
  </animation>
 </tile>
 <tile id="45">
  <objectgroup draworder="index">
   <object id="1" x="10.75" y="15.125" width="10.375" height="11.375"/>
  </objectgroup>
 </tile>
 <tile id="50" type="attack_l">
  <objectgroup draworder="index">
   <object id="1" x="12" y="16" width="8.125" height="10.5"/>
  </objectgroup>
  <animation>
   <frame tileid="50" duration="100"/>
   <frame tileid="51" duration="100"/>
   <frame tileid="54" duration="100"/>
  </animation>
 </tile>
 <tile id="55" type="walk_l">
  <objectgroup draworder="index">
   <object id="1" x="12" y="18" width="7" height="8"/>
  </objectgroup>
  <animation>
   <frame tileid="55" duration="100"/>
   <frame tileid="56" duration="100"/>
   <frame tileid="55" duration="100"/>
   <frame tileid="58" duration="100"/>
  </animation>
 </tile>
 <tile id="60" type="idle_l">
  <objectgroup draworder="index">
   <object id="1" x="12" y="18" width="8" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="55" duration="100"/>
   <frame tileid="55" duration="100"/>
   <frame tileid="55" duration="100"/>
   <frame tileid="61" duration="100"/>
  </animation>
 </tile>
 <tile id="65" type="pull_l">
  <objectgroup draworder="index">
   <object id="1" x="12" y="18" width="8" height="10"/>
  </objectgroup>
  <animation>
   <frame tileid="65" duration="100"/>
   <frame tileid="66" duration="100"/>
  </animation>
 </tile>
 <tile id="70" type="receive"/>
 <tile id="71" type="falling">
  <animation>
   <frame tileid="71" duration="100"/>
   <frame tileid="72" duration="100"/>
   <frame tileid="73" duration="100"/>
   <frame tileid="73" duration="100"/>
   <frame tileid="73" duration="100"/>
  </animation>
 </tile>
 <tile id="75" type="die">
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="55" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="55" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="50" duration="100"/>
   <frame tileid="75" duration="100"/>
   <frame tileid="75" duration="100"/>
   <frame tileid="75" duration="100"/>
   <frame tileid="75" duration="100"/>
  </animation>
 </tile>
</tileset>
