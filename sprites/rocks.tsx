<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.09.12" name="rocks_53x27" tilewidth="53" tileheight="27" tilecount="4" columns="1">
 <image source="rocks_53x27.png" trans="003039" width="53" height="108"/>
 <tile id="0" type="rock1"/>
 <tile id="1" type="rock2"/>
 <tile id="2" type="rock3"/>
 <tile id="3" type="rock4"/>
</tileset>
