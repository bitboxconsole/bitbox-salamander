<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.09.12" name="house_roof_red" tilewidth="10" tileheight="43" tilecount="2" columns="2">
 <image source="house_roof_red.png" trans="003039" width="20" height="43"/>
 <tile id="0" type="left"/>
 <tile id="1" type="right"/>
</tileset>
