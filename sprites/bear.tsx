<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="bear" tilewidth="32" tileheight="32" tilecount="24" columns="6">
 <image source="bear_32x32.png" trans="d92ccf" width="192" height="128"/>
 <tile id="0" type="sleep">
  <animation>
   <frame tileid="0" duration="1000"/>
   <frame tileid="1" duration="133"/>
   <frame tileid="2" duration="133"/>
   <frame tileid="3" duration="500"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="1" duration="250"/>
  </animation>
 </tile>
 <tile id="6" type="standup">
  <animation>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
   <frame tileid="8" duration="100"/>
   <frame tileid="13" duration="100"/>
  </animation>
 </tile>
 <tile id="12" type="walk_r"/>
 <tile id="13" type="walk_r">
  <animation>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="15" duration="100"/>
  </animation>
 </tile>
 <tile id="18" type="attack_r">
  <animation>
   <frame tileid="18" duration="100"/>
   <frame tileid="19" duration="100"/>
   <frame tileid="20" duration="100"/>
   <frame tileid="21" duration="100"/>
   <frame tileid="22" duration="100"/>
   <frame tileid="23" duration="100"/>
   <frame tileid="21" duration="100"/>
   <frame tileid="22" duration="100"/>
   <frame tileid="23" duration="100"/>
   <frame tileid="20" duration="100"/>
   <frame tileid="21" duration="100"/>
   <frame tileid="22" duration="100"/>
   <frame tileid="19" duration="100"/>
   <frame tileid="18" duration="100"/>
  </animation>
 </tile>
</tileset>
