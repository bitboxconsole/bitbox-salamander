<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="rat" tilewidth="22" tileheight="22" tilecount="32" columns="4">
 <properties>
  <property name="update" value="rat"/>
 </properties>
 <image source="rat.png" trans="ff00da" width="88" height="176"/>
 <tile id="0" type="walk_r">
  <properties>
   <property name="update" value="rat_walk"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="7" y="7" width="13" height="8"/>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
  </animation>
 </tile>
 <tile id="3">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="4" type="walk_l">
  <properties>
   <property name="update" value="rat_walk"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="6" width="15" height="8"/>
  </objectgroup>
  <animation>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
  </animation>
 </tile>
 <tile id="7">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="8" type="walk_up">
  <properties>
   <property name="update" value="rat_walk"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="7" y="2" width="7" height="12"/>
  </objectgroup>
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
  </animation>
 </tile>
 <tile id="12" type="walk_dn">
  <properties>
   <property name="update" value="rat_walk"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="5" y="8" width="10" height="14"/>
  </objectgroup>
  <animation>
   <frame tileid="12" duration="100"/>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="15" duration="100"/>
  </animation>
 </tile>
 <tile id="16" type="pause_r">
  <properties>
   <property name="update" value="rat_pause"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="6" y="6" width="14" height="10"/>
  </objectgroup>
  <animation>
   <frame tileid="16" duration="100"/>
   <frame tileid="16" duration="100"/>
   <frame tileid="22" duration="100"/>
  </animation>
 </tile>
 <tile id="17" type="pause_l">
  <properties>
   <property name="update" value="rat_pause"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="1.81818" y="7" width="13.1818" height="9"/>
  </objectgroup>
  <animation>
   <frame tileid="17" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="23" duration="100"/>
  </animation>
 </tile>
 <tile id="18" type="pause_up">
  <properties>
   <property name="update" value="rat_pause"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="6" y="1" width="9" height="11"/>
  </objectgroup>
 </tile>
 <tile id="19" type="pause_dn">
  <properties>
   <property name="update" value="rat_pause"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="5" y="8" width="9" height="12"/>
  </objectgroup>
 </tile>
 <tile id="20" type="killed">
  <animation>
   <frame tileid="20" duration="133"/>
   <frame tileid="21" duration="133"/>
   <frame tileid="21" duration="133"/>
   <frame tileid="21" duration="133"/>
  </animation>
 </tile>
 <tile id="21">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="24" type="sleeping">
  <properties>
   <property name="update" value="rat_sleep"/>
  </properties>
  <animation>
   <frame tileid="24" duration="100"/>
   <frame tileid="24" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="25" duration="100"/>
  </animation>
 </tile>
 <tile id="25">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="update" value="rat"/>
  </properties>
 </tile>
</tileset>
