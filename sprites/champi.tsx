<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="champi_16x16" tilewidth="16" tileheight="16" tilecount="6" columns="6">
 <image source="champi_16x16.png" width="96" height="16"/>
 <tile id="0" type="idle">
  <properties>
   <property name="update" value="getit"/>
  </properties>
  <animation>
   <frame tileid="0" duration="120"/>
   <frame tileid="1" duration="120"/>
   <frame tileid="2" duration="120"/>
   <frame tileid="3" duration="120"/>
   <frame tileid="4" duration="120"/>
   <frame tileid="5" duration="120"/>
   <frame tileid="1" duration="120"/>
   <frame tileid="0" duration="120"/>
   <frame tileid="0" duration="130"/>
   <frame tileid="0" duration="100"/>
  </animation>
 </tile>
</tileset>
