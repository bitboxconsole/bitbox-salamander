<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="treetop" tilewidth="64" tileheight="64" tilecount="2" columns="2">
 <grid orientation="orthogonal" width="48" height="48"/>
 <image source="treetop_64x64.png" trans="ff00da" width="128" height="64"/>
 <tile id="0" type="tree1">
  <objectgroup draworder="index">
   <object id="1" x="24" y="38" width="17" height="21"/>
  </objectgroup>
 </tile>
 <tile id="1" type="tree2">
  <objectgroup draworder="index">
   <object id="1" x="20" y="32" width="24" height="28"/>
  </objectgroup>
 </tile>
</tileset>
