<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="room_stair" tilewidth="26" tileheight="15" tilecount="1" columns="1">
 <image source="room_stair.png" trans="ff00da" width="26" height="15"/>
 <tile id="0" type="stair">
  <properties>
   <property name="update" value="sendto"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="2" y="1" width="22" height="14"/>
  </objectgroup>
 </tile>
</tileset>
