<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="ladder_outside" tilewidth="32" tileheight="16" tilecount="1" columns="1">
 <image source="ladder_outside.png" trans="003039" width="32" height="16"/>
 <tile id="0" type="_">
  <properties>
   <property name="update" value="sendto"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="10" y="6" width="12" height="7"/>
  </objectgroup>
 </tile>
</tileset>
