<?xml version="1.0" encoding="UTF-8"?>
<tileset name="bonh" tilewidth="32" tileheight="32" tilecount="32" columns="8">
 <image source="bonh.png" trans="ff00da" width="256" height="128"/>
 <tile id="0" type="walk_r">
  <properties>
   <property name="state" value=""/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
  </animation>
 </tile>
 <tile id="8" type="attack_r">
  <properties>
   <property name="state" value=""/>
  </properties>
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
  </animation>
 </tile>
 <tile id="16" type="walk_up">
  <properties>
   <property name="state" value=""/>
  </properties>
 </tile>
</tileset>
