<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="items16" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="items16.png" trans="ff00da" width="128" height="128"/>
 <tile id="0" type="letter"/>
 <tile id="1" type="transp"/>
 <tile id="2" type="chest_open"/>
 <tile id="3" type="chest_closed">
  <properties>
   <property name="update" value="chest"/>
  </properties>
 </tile>
 <tile id="4" type="sign">
  <properties>
   <property name="update" value="sign"/>
  </properties>
 </tile>
 <tile id="8" type="vase">
  <objectgroup draworder="index">
   <object id="1" x="1" y="2" width="14" height="13"/>
  </objectgroup>
 </tile>
 <tile id="9" type="vasebroken">
  <animation>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
  </animation>
 </tile>
 <tile id="11" type="send">
  <properties>
   <property name="update" value="sendto"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="17" type="wine">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="19" type="say">
  <properties>
   <property name="update" value="say"/>
  </properties>
 </tile>
 <tile id="24" type="bomb">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="25" type="entry"/>
 <tile id="26">
  <objectgroup draworder="index">
   <object id="1" x="0.5" y="5.5" width="14.75" height="9.5"/>
  </objectgroup>
 </tile>
 <tile id="32" type="light">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="33" type="punch">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="34" type="ring">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="35" type="key">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="36" type="feather">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="37" type="shovel">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="38" type="hammer">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="39" type="bow">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="40" type="wand">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="56" type="sword_stick">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="57" type="sword_iron">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="58" type="sword_gold">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="59" type="sword_magic">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="update" value="getit"/>
  </properties>
 </tile>
</tileset>
