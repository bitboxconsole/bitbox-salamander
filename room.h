/* A room is autonomous. 
   the only way out of a room is by calling its exit()
   it handles all RAM data necessary for running.
 */

#pragma once
#define MAX_OBJECTS 128 // in a room (in RAM)

#include "object.h"
#include "player.h"

#include "defs.h"

class Room {
public:

	void load(int room_id, int entry);
	void unload();

	void bg_scroll();
	CollisionType bg_property_at(int x, int y) const; // return CollisionType

	Collision bg_collide(const Object &object) const; // object collision state for each corner of the sprite

	void frame(); // update for frame

	Object &new_object(const ObjectType &objtype,const uint8_t _value);
	void fadeIn();
	void fadeOut();

	bool night;

	Player player; // The player object within the room

protected:
	const RoomDef *def;

	// load objects and position player at entry X
	void load_objects(
		const TilemapFileObject *obj, 
		const int entry 
		);

	void load_tilemap (const int entry);

	object background, darken;
	uint8_t *tmap;
	
	Object objects[MAX_OBJECTS]; // objects, maybe active (when update != 0)
};
