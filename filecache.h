#pragma once

// filecache
/* load files, ensure they're present in memory, remove them if cached, refcounted */
// public API
#include <stdint.h>

extern "C" {
#include "sdk/kernel/fatfs/ff.h"
#include "sdk/kernel/bitbox.h"
#include "sdk/lib/resources/tinymalloc.h"
}

#include "defs.h"

#define DEBUG 0

// -- globals
template <int cache_size, int nbfiles>
class FileCache {
public: 
	FileCache() {
		// init tmalloc
		t_addchunk(ram,cache_size);

		// mount / open file
		if (f_mount (&fatfs,"",1) || (f_open (&sd_file, DATA_FILENAME,FA_READ))) {
			message("could not open TAR file"); 
			die(8,4);
		}
		message("filecache init ok\n");
	}

	void *load_file(uint16_t file_id)
	{
		if (DEBUG) message("loading file #%d ...", file_id);
		if (!file_id) {
			message("refusing to load file id 0\n");
			die(0,1);
		}

		int idx=find_file(file_id); // find in local table
		if (idx>=0) {
			if (DEBUG) message("already in memory\n");
			files[idx].refcnt++;
			return 	files[idx].ptr;
		} 

		// get free place
		do {
			idx=find_file(0); // find free ie file_id zero
		} while (idx<0 && clean());

		if (idx<0) {
			message ("could not get free file entry");
			die(8,0);
		}

		// find file reference in file table
		const uint16_t block = data_files[file_id][0];
		const uint16_t sz  = data_files[file_id][1];
		if (DEBUG) message (": will load file block %d, size %d - ",block,sz);

		// find free memory
		void *ptr;
		do {
			ptr = t_malloc(sz); // catch error	
		} while (!ptr && clean());
		
		if (!ptr) {
			message("Could not get free memory, dying.\n");
			t_print_stack();
			die(8,1); // not enough memory cleanble
		}

		// load file
		UINT read_bytes;
		// filename 
		if (DEBUG) {
			f_lseek(&sd_file, block*512-512);
			f_read (&sd_file, ptr, 32, &read_bytes); 
			message("Read file: %s\n",data_filenames[file_id]);
		}
		
		// read the file
		f_lseek(&sd_file, block*512);
		f_read (&sd_file, ptr, sz, &read_bytes); 

		if (read_bytes != sz) {
			message("WARNING : file not completely loaded ! Error \n");
		}

		files[idx].ptr = ptr;
		files[idx].refcnt = 1;
		files[idx].file_id = file_id;
		if (DEBUG)
			message("Done reading file %s at pos %d\n",data_filenames[file_id], idx);

		return ptr;
	}

	// free file from data pointer
	void free_file(const void *file_data)
	{
		message("free file ptr %p\n", file_data);
		uint8_t idx = find_ptr(file_data);
		if (idx<0) {
			message("file %d not found\n", idx);
			die(8,4); // file no found or already ded
		}
		free_file_idx(idx);
	}

	// free file from file_id
	void free_file(const uint16_t file_id)
	{
		message("free file id %d\n", file_id);
		uint8_t idx = find_file(file_id);
		if (idx<0) {
			message("file %d not found\n", idx);
			die(8,4); // file no found or already ded
		}
		free_file_idx(idx);
	}

	// list files status in memory
	void list_files()
	{
		int cached_size=0;
		int free_files=0;
		int cached_files=0;
		int nb_files=0;
		int loaded_size=0;

		for (int i=0;i<nbfiles;i++) {
			if (files[i].file_id == 0) {
				free_files++;
			} else {
				message(" %d -  %.20s (%d bytes, refcnt %d, ptr %p)\n", 
					i,
					data_filenames[files[i].file_id], 
					data_files[files[i].file_id][1],
					files[i].refcnt,
					files[i].ptr
					);

				if (files[i].refcnt==0) {
					cached_size += data_files[files[i].file_id][1];
					cached_files++;
				} else {
					loaded_size += data_files[files[i].file_id][1];
					nb_files ++;
				}
			}
		}
		message ("  %d files loaded (%d bytes)\n", nb_files, loaded_size);
		message (
			"  free files: %d (%d B.), cached:%d (%d B.), total:%d(%d B.)\n", 
			free_files, t_available(),
			cached_files, cached_size, 
			free_files+cached_files, t_available()+cached_size
			);
	}

protected: 
	/*  a list of open files with a reference counter */
	struct { 
		uint16_t file_id; // file id. zero means free, nonzero means there is data 
		uint8_t refcnt; // zero with file_id != 0 means still cached but data still here
		void *ptr; 
	} files[nbfiles];
 	FIL sd_file;
	FATFS fatfs;

	char ram[cache_size];

	int find_file(uint16_t file_id) const // return index in local table or -1
	{
		for (int i=0;i<nbfiles;i++)
			if (files[i].file_id == file_id)
				return i;
		return -1; // not found
	}
	
	int find_ptr(const void *ptr) const // return index in local table or -1
	{
		for (int i=0;i<nbfiles;i++)
			if (files[i].ptr == ptr)
				return i;
		return -1; // not found
	}

	int clean() 
	// effectively free an unused file from memory. return 1 if was possible to clean
	{
		if (DEBUG) {
			message("   trying to clean...\n");
			list_files();
		}

		// find a file of refcnt zero, with file_id not zero.
		for (int i=0;i<nbfiles;i++)
			if (files[i].refcnt==0 && files[i].file_id != NOFILE) { // take first ..
				t_free(files[i].ptr); // free memory			return 1;
				message("   cleaned file %d (%s): %d bytes left now\n", i, data_filenames[files[i].file_id], t_available());
				files[i].file_id = NOFILE;
				files[i].ptr = 0;
				return 1;
			}
		return 0; // not found
	}

	void free_file_idx(const uint8_t idx)
	{
		if (!files[idx].refcnt) {
			message("ERROR: file %d already dead\n", idx);
			die(8,4); // file no found or already ded
			while(1);
		}
		files[idx].refcnt--; // dont reclaim memory yet, maybe keep in cache for later
		if (DEBUG) message("file index %d (file_id %d) now refcnt %d\n",idx,files[idx].file_id, files[idx].refcnt);
	}
};

