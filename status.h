#pragma once
#include "room.h"

#include "defs.h" // room_id

enum {
	item_letter,
	item_light,
	item_punch,
	item_ring,
	item_key,
	item_feather,
	item_shovel,
	item_hammer,
	item_bow,
	item_wand,
	item_sceptre_red,
	item_sceptre_green,
	item_sceptre_blue,

	NB_ITEMS
}; 

// weapons objects
enum {
	sword_none,
	sword_stick,
	sword_metal,
	sword_enchanted,
};

// global status. a dataclass
struct Status {

	uint8_t room;
	uint8_t entry;

	uint8_t life; // shown as half-hearts
	uint8_t life_max;
	uint8_t mana; // 0-7
	uint8_t mana_max; // 0-7

	// resources inventory
	uint8_t gold;
	uint8_t bombs;
	uint8_t arrows;
	uint8_t mushrooms;

	// replaced items
	uint8_t sword; // different types, only one at a time. see sword enum
	uint8_t shield; // different types, only one at a time. see sword enum

	unsigned items: NB_ITEMS;

	// unsigned keys:2; // or an item / dungeon item

	// switches
	unsigned night:1;
	unsigned town_nuit_guard_talked: 2;
	unsigned start_sub_shown_msg:1;
	unsigned start_laby_talked_old:1;
	unsigned forest_talked_father:1;


	Status() : 
		room(room_forest),
		entry(0),

		life(3), 
		life_max(6), 
		mana(1), 
		mana_max(4), 

		gold(17), 
		bombs(8), 
		arrows(16),
		mushrooms(0),

		sword(sword_stick), 

		items((1<<NB_ITEMS)-1),
		night(0)
		{}
};
