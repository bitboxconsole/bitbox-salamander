#!/usr/bin/python3

"tar to X macro header file"
import tarfile, sys

print ('#pragma once')

print ('// X macro : X(name, block_id, size)')
print ('#define FILE_LIST_XMACRO \\')
members = tarfile.open(sys.argv[1]).getmembers()

unique_names = set()
for info in members : 
	quoted = info.name[5:].replace('.','_')
	print ('\tX( {:30s}, {:4d},{:4d}) \\'.format(quoted,info.offset_data//512,info.size))
	unique_names.add(quoted)
print()
assert len(members)==len(unique_names)