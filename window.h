#pragma once
#include "sdk/lib/blitter/blitter.h"

// Window, used to show messages 
#include "status.h" 

#define WINDOW_SPEED 5 // how fast the window appears
#define MESSAGE_SPEED 4 
#define SURFACE_WIDTH 160

#define HUD_W 22
#define HUD_H 2

#define HUD_X ((40-HUD_W)/2)
#define HUD_Y (15-HUD_H)

class Window : public object // it's a tilemap (of hearts and magic points)
{
public:
	Window();
	
	void update_hud();
	int  dialog (const int face_id,const char *msg,const char *answers); // 0 = message
	void inventory (Status &status);

protected:

	void set_inventory(int y); // move hud and set surface to inventory
	void set_message(int y);    // move message and set surface to message

	uint8_t display[HUD_W*2]; // 20 wide
	
	/*
	// draws item item_id of line line to vram x,y 
	void draw_item(int x,int y,int line, int item_id);
	void draw_window (int x1, int y1, int x2, int y2);
	*/
	TilemapFile *tmap;
	
	object window_bg;
	object window_tmap_object;
	object surface;
	object message_bg;
	object face;

	char surface_data[SURFACE_BUFSZ(SURFACE_WIDTH,32)]; 

private:
	void text_slow (const char *text, int x, int y);

	// singleton 
	static Window &instance() {
		static Window win;
		return win;
	}
};
