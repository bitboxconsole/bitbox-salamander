/* TODO : 

- window display : dialogs a revoir (progressif, borders collide, button skip)
- hud
- hold objects  
- shared tilesets
- nonclipped lines : custom blitter lib
- sfx hit, white flash ? 
- single music

*/
#include "bitbox.h"
#include "miniz.h"

#define DEFS_IMPLEMENTATION
#include "defs.h"
#undef DEFS_IMPLEMENTATION

#include "game.h"


// The game global
Game game;

Game::Game()
{
}

void Game::play()
{
	room.load(status.room, status.entry); // take initial values from status
	
	window.update_hud();

	while(1) {
		if (GAMEPAD_PRESSED(0,start)) {
			window.inventory(status);
		}
		room.frame();
		wait_vsync();
	}
}

void wait_vsync (int n) { for (int i=0;i<n;i++) wait_vsync(); }

extern "C" {
	void bitbox_main( void )
	{
		game.play();
	}
}