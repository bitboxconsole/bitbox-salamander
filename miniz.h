#pragma once
#include "bitbox.h"
#define _(x) x

#define SILENT 1

#define DIE(a,b,msg) do{ message("ERROR %s:%d %s", __FILE__, __LINE__ );die(a,b) } while (0)


enum class Dir {
	down, left, right, up, none // in that order so they match alphabetical order for files
};

/*
enum {
	shield_none,
	shield_wood,
	shield_metal,
	shield_enchanted,
};
*/

void wait_vsync(int n);
