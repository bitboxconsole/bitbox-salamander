#include <cstdlib> // rand
#include "miniz.h" // _()
#include "defs.h"
#include "game.h"

void update_dad(Object *o, Collision col)
{

	if (o->state() == pnj_dad_sit_spr) {
		if (col) {
			game.window.dialog(faces_dad_spr,_("My son, I really need those\n20 mushrooms. Can you bring me some more ?"),nullptr);
		}
		return;
	}

	o->value--; // a frame counter
	
	if ((o->state() == pnj_dad_walk_r_spr || o->state() == pnj_dad_walk_l_spr )) {
		if (o->life%2) {
			o->x--;
			o->y += o->life>>1;
		} else {
			o->x++;
			o->y += o->life>>1;
		}
	} else if (col) { // collide only when not walking
		game.window.dialog(faces_dad_spr,_("Hi son ! How are you ?\nWhat are you doing out so late?"),nullptr);
		game.window.dialog(faces_dad_spr,_("Oh well, since you're here, help me find moon mushrooms.\nThey're best plucked on a new moon."),nullptr);
		game.window.dialog(faces_dad_spr,_("I've hurt myself anyway.\nWhen you've got 20, please bring them back to me. I'll wait for you here."),nullptr);
		game.status.forest_talked_father = 1;

		// go right and sit down
		o->state(pnj_dad_walk_r_spr);
		for (int i=0;i<24;i++) {
			o->x++;
			wait_vsync(1);
			if (i%8==0) 
				o->fr = 1-o->fr; // alternate
		}
		o->state(pnj_dad_sit_spr);
	}

	// state machine : walk / pick / wait
	if (o->value) return;
	switch(o->state()) {
		case pnj_dad_walk_r_spr : 
		case pnj_dad_walk_l_spr : 
			o->state(pnj_dad_pick_spr);
			o->value = 6*8;
			break;
		case pnj_dad_pick_spr : 
			// pick a direction speed (x/y), a length and set to value = 
			o->state(pnj_dad_wait_spr);
			o->value = 128;
			break;
		case pnj_dad_wait_spr : 
			o->value = rand()%100;
			o->life = rand()%7-3;
			o->state(o->life%2 ? pnj_dad_walk_l_spr : pnj_dad_walk_r_spr);
			break;
	}
}
 
void update_bee(Object *o, Collision col)
{
	if (rand()%2) o->x++;
	if (rand()%2) o->x--;
	if (rand()%2) o->y++;
	if (rand()%2) o->y--;
	// dont go out of the beehive zone
	// attack player if in the beehive zone
}
