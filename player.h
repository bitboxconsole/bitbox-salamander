#pragma once
#include "miniz.h"
#include "object.h"
#include "defs.h"

class Player : public Object
{
public:
	Player();

	void obj_collide( Collision *q );
	void control(void);

	void fall_anim();
	void take_anim(int file_id);
	void die_anim(); // die animation

	void take(int item_id);

	Object sword; // currently equipped sword. 
	Object *hold;  // pulling / lifting something
	
	void invincible(); // make it blink if invincible ... 
	void walk(Dir dir); // start walking in a direction
	void idle(); // rest
	void pull(Object *o); // pulls an object
	void hurt(int damage); // hurts the player
	void sword_attack();
	void update_sword ();

	Dir dir() { return static_cast<Dir>(value);} // facing direction, not moving
	void dir(Dir dir) { value=static_cast<int>(dir);}
	void state_dir(FilesEnum _file_id) { 
		message("state dir %d %d \n", _file_id, value);
		state(_file_id+value); 
	} // files in order & correspond to dir enum order

protected:
	static void update_idle  (Object *self, Collision c);
	static void update_walk  (Object *self, Collision c);
	static void update_pull  (Object *self, Collision c);
	bool still_pressed(); // faced direction still pressed ?

	uint8_t invincibility_frames;
};
