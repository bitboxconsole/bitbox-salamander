// objects behaviours

#include <stdlib.h> // rand
#include "behaviours.h"
#include "room.h"
#include "game.h"

void update_idle (Object *, Collision col)  {} // do nothing.


// --- Utilities ---------------------------------------------------------------------
static void can_hurt(Collision col, int damage) {
	if (col) game.room.player.hurt(damage);
}

static void can_be_hurt(Object *o, int8_t initial_life, int death_file_id)
{
	if (game.room.player.sword.test_collide(*o)) { 
		if (o->life==-1)
			o->life = initial_life;
		o->life--;	// FIXME depends on sword type / enemy
		if (o->life<=0) {
			o->state(death_file_id);
			o->update = update_playonce;
		}
	}
}
// --- Monsters ---------------------------------------------------------------------
/* Rat : 

go straight. 
if collides with bg or after a given time :
    pauses a bit 
    start again in a random direction
hurt player if touch it

value = frames of pause left.
*/
void update_rat_pause (Object *o, Collision col)
{	
	can_be_hurt(o,2,rat_killed_spr);
	can_hurt(col,1);
	if (!o->value--) {
		o->value = rand() % 50; // frames for walk
		o->state(rat_walk_dn_spr+rand()%4);
		o->update = update_rat_walk;
	}
}

void update_rat_walk (Object *o, Collision col)
{	
	can_be_hurt(o,2,rat_killed_spr);
	can_hurt(col,1);
	switch(o->state()) {
		case rat_walk_up_spr : o->y--; break;
		case rat_walk_dn_spr : o->y++; break;
		case rat_walk_l_spr  : o->x--; break;
		case rat_walk_r_spr  : o->x++; break;
	}

	if (!o->value-- || game.room.bg_collide(*o)) {
		o->value = rand() % 128; // frames for pause
		o->state(rat_pause_dn_spr + rand()%4);
		o->update = update_rat_pause;
	}
}

void update_rat_sleep (Object *o, Collision col) 
{
	can_be_hurt(o,3,rat_killed_spr);
	can_hurt(col,1);
}


// --- Generics ---------------------------------------------------------------------


// play animation once then destroy object
void update_playonce(Object *o, Collision col) {
	if (vga_frame%8==0 && o->fr == o->def()->frames-1) {
		o->destroy();
	}
}

// when hit, send to entry X (defined in room TMX file sendto information)
void update_sendto (Object *o, Collision col) {
	Room &room = game.room;
	const int current = game.status.room;
	if (col) {
		// room.exit(o->value) ? 
		message("sendto: from room %d exit %d\n",current, o->value);

		// find in room_id,exit_id -> room_id, entry, fade
		const int nb = sizeof(room_exits)/sizeof(room_exits[0]);
		for (unsigned int  i=0;i<nb;i++) {
			const auto exit = room_exits[i];
			if (exit.from == current && exit.exit == o->value) {

				// exit graphics -> to room.unload
				switch (exit.type) {
					case SendtoType::fade : room.fadeOut(); break;
					case SendtoType::fall : room.player.fall_anim(); break;
					case SendtoType::normal : break;
				}

				room.unload();
				room.load(exit.to,exit.entry); 
				if (room_exits[i].type == SendtoType::fade) 
					room.fadeIn();

				return;
			}
		}
		message("could not find sendto info for %d/%d\n", current, o->value);
		die(7,7);
	}
}

// coffer. can react to being opened by giving item in value - if not already opened !
void update_chest(Object *o, Collision col)
{
	const uint16_t items_chest[] = {
		[0] = items16_letter_spr,
	}; // value -> object inside

	if (col && GAMEPAD_PRESSED(0,B)) {
		o->state(items16_chest_open_spr); // open it
		o->update = update_idle; // cannot have it anymore
		game.room.player.take(items_chest[o->value]);
	}
}

void update_getit(Object *o, Collision col)
{
	if (col) {
		int file_id = o->file_id; // save it
		o->destroy();
		game.room.player.take(file_id);
	}
}

void update_canpull (struct Object *o, Collision col)
{
	// can pull if empty hands and use button
	if (col && GAMEPAD_PRESSED(0,B) && !game.room.player.hold) {
		game.room.player.pull(o);
	}
}
