#pragma once

#include "status.h"
#include "room.h"
#include "window.h"
#include "filecache.h"

class Game
{
public:
	Game();

	FileCache<20000, 32> files;
	Status status;
	Room   room;
	Window window;

	void play();

};

extern Game game;
