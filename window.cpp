//#include <bitbox.h>

#include "string.h"
#include "miniz.h"
#include "gamepad.h"

#include "lib/blitter/blitter.h"
#include "data.h"

#include "window.h"
#include "game.h"

static const int x_hud = 64;
static const int y_hud = -100;
static const int y_message = VGA_V_PIXELS-48-4;
const pixel_t inventory_palette[4] = {2,7,15,223};
const pixel_t message_palette[4]   = {180,224,139,65};
const pixel_t dialog_palette[4]    = {181,148,139,33};

static auto font = reinterpret_cast<const Font*>(data_mini_fon);

Window::Window() 
{
	// create HUD tset / tmap
	auto hud_tset = reinterpret_cast<const TilesetFile*>(data_window_hud_tset);
	if (hud_tset->tilesize != 8) die(2,1);
	if (hud_tset->datacode != 1) die(2,2);

	tilemap_init(&this->window_tmap_object, 
		hud_tset->data,
		0,0, 
		TMAP_HEADER(HUD_W,2,TSET_8,TMAP_U8) | TSET_8bit,
		&display
		);

	for (int i=0;i<HUD_W*2;i++) 
		display[i]=0;

	// sprite bg
	sprite3_load(&window_bg, data_window_bg_spr);
	sprite3_load(&message_bg, data_message_bg_spr);

	// text surface
	surface_init (&surface, SURFACE_WIDTH, 32, &surface_data);

	blitter_insert(&message_bg , x_hud   , 1024, -210);
	blitter_insert(&window_bg , x_hud   , y_hud, -210);
	blitter_insert(&window_tmap_object, x_hud+8 , y_hud+104, -211);
	blitter_insert(&surface , x_hud+8 , 1024, -211);
	blitter_insert(&face , x_hud+8 , 1024, -212);
}


void Window::update_hud() // (re)draw hud
{
	const Status &status = game.status;
	message("updated hud\n");
	// draw gold, bomb, arrows
	if (status.gold >10)
		display[HUD_W+0] = status.gold/10+1;
	display[HUD_W+1] = status.gold%10+1;

	display[HUD_W+3] = status.bombs%10+1;
	
	if (status.arrows>10)
		display[HUD_W+5] = status.arrows/10+1;
	display[HUD_W+6] = status.arrows%10+1;

	display[HUD_W+8] = status.mushrooms/10+1;
	display[HUD_W+9] = status.mushrooms%10+1;

	// lives 
	int i;
	for (i=0;i<status.life/2;i++)
		display[12+i] = 11;
	if (status.life%2) {
		display[12+i] = 12;
		i++;
	}
	for (;i<status.life_max/2;i++)
		display[12+i] = 13;

	// mana
	for (i=0;i<status.mana/2;i++)
		display[18+i] = 14;
	if (status.mana%2) {
		display[18+i] = 15;
		i++;
	}
	for (;i<status.mana_max/2;i++)
		display[18+i] = 16;


	// sword / shield
}

void Window::set_inventory(int y) 
{
	window_bg.y = y;
	window_tmap_object.y = window_bg.y + 104;
	surface.y = window_bg.y + 75;
}


void Window::inventory(Status &status)
{
	surface.h = 20;
	surface.x = x_hud+8;
	surface_setpalette(&surface,inventory_palette);

	surface_clear(&surface);
	surface_text(&surface, "Coucou les amis !\nCeci est l'inventaire.",0,0,font);

	for (int i=0;i<25;i++) {
		set_inventory(y_hud+WINDOW_SPEED*i);
		wait_vsync();
	}

	// select, show description, ...

	// wait for re-press ENTER to leave
	while (Gamepad::wait_pressed() != gamepad_start);

	for (int i=24;i>=0;--i) {
		set_inventory(y_hud+WINDOW_SPEED*i);
		wait_vsync();
	}
}

void Window::set_message(int y) 
{
	message_bg.y = y;
	surface.y = message_bg.y + 8;
}

void Window::text_slow (const char *text, int x, int y)
{
	int cx = x; // current X
	for (const char *c=text ; *c ; c++) {
		if (y+font->height > surface.h)
			break;

		if (*c =='\n') {
			y += font->height+1;
			cx = x;
		} else if (*c=='\t') {
			cx = (cx+32)/32*32;
		} else if (*c==' ') {
			cx += 2;
		} else {
			if (cx>surface.w-4) {
				y+=font->height+1;
				cx=x;
			}
			cx += surface_char(&surface, *c, cx, y, font)+1;
		}
		wait_vsync(Gamepad::attack() ? 1 : MESSAGE_SPEED);
	}
}


// face_id : 0 if none, file_id else
// msg : \n separated multiline message.
// answers: \n separated different answers, NULL : this is a message. 
int Window::dialog (const int face_id, const char *msg, const char *answers)
{

	surface.x = x_hud+16;
	surface.h = 32;
	surface_setpalette(&surface,message_palette);

	surface_clear(&surface);
	
	if (face_id) {
		surface_setpalette(&surface, dialog_palette);
		surface.x = x_hud+38;
		message("loading face %s\n", data_filenames[face_id]);
		sprite3_load(&face, game.files.load_file(face_id));
		face.x = x_hud+5;
		sprite3_load(&message_bg, data_talk_bg_spr );
	} else {
		surface_setpalette(&surface, message_palette);
		surface.x = x_hud+8;
		sprite3_load(&message_bg, data_message_bg_spr );
		face.y = 1024;
	}

	for (int i=24;i>=0;i--) {
		set_message(y_message+i*WINDOW_SPEED);
		if (face_id) face.y = y_message+4+i*WINDOW_SPEED;
		wait_vsync();
	}

	// display message
	if (msg) {
		text_slow (msg,0,0);
	}

	if (!answers) {
		answers = "Ok";
	}
	// count choices
	int nchoices=1;
	for (const char *c=answers;*c;c++) {
		if (*c=='\n')
			nchoices++;
	}
	// display answers, another color? a little right
	const int ans_x = 38;
	const int ans_y = 32-9*nchoices+1;
	message("nchoices %d y %d\n", nchoices, ans_y);

	surface_text(&surface, answers,ans_x,ans_y,font); 

	int choice=0; // current choice
	// select, display cursor
	// Make it a blinking sprite ?
	surface_char(&surface, '~'+1,ans_x-8,ans_y+9*choice, font);
	while(1) {
		uint16_t btn = Gamepad::wait_pressed();
		// clear it
		surface_fillrect(&surface, ans_x-12,ans_y+9*choice,ans_x-1,ans_y+9*choice+8,0);
		switch (btn) {
			case gamepad_down : 
				if (++choice >= nchoices) 
					choice = 0;
				break;
			case gamepad_up : 
				if (choice == 0) 
					choice = nchoices -1;
				else 
					choice -=1;
				break;
			case gamepad_A : 
				goto end; // yay, a goto !
				break;
		}
		surface_char(&surface,'~'+1,ans_x-8,ans_y+9*choice, font);
	}

end: 
	for (int i=0;i<24;++i) {
		set_message(y_message+i*WINDOW_SPEED);
		if (face_id) face.y = y_message+4+i*WINDOW_SPEED;
		wait_vsync();
	}
	// release face
	if (face_id) 
		game.files.free_file(face_id);

	return choice;
}
