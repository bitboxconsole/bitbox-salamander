#!/usr/bin/python3
'''
  Generates all sprites files from scanning tmx files.
'''
import sys
import os
import xml.etree.ElementTree as ET
import logging
import tarfile
from collections import OrderedDict

sys.path.append('sdk/lib/blitter/scripts')
from mk_tmap import out_objects

DATA_FILENAME="salamander.tar"
ROOMS = [
    'start',
    'start_town_night',
    'start_underground',
    'start_labyrinth',
    'town',
    'beach',
    'bateau',
    'forest',
    ]

maps = []
spritesheets = set(['sprites/faces.tsx'])
musics = set()
exits = [] # sendto info : start_room, start_exit_id, to_room, to_entry_id, type

def get_property(elt, property, default) :
    prop = elt.find('./properties/property[@name="%s"]'%property)
    return prop.get('value') if prop is not None else default

for room in ROOMS :
    mapfile = 'rooms/'+room+'.tmx'
    map = ET.parse(mapfile).getroot()

    # tileset, tilemap
    for x in map.findall('./tileset[@source]') :
        src = x.get('source')
        if not src.startswith('../sprites') : # tileset ?
            tileset = src # only one
        else : 
            spritesheets.add(src[3:]) # sprites/XXX.tsx
    
    # global map properties
    music = get_property(map,'music',0)
    if music : musics.add(music)

    # exits
    for x in map.findall('./objectgroup/object/properties/property[@name="sendto"]/../..') :
        exit_name = x.get('name')
        exit_props = x.find('properties/property[@name="sendto"]').get('value').split() # name, entry, type
        exits.append((room, exit_name, *exit_props))

    # room data : tilemapfile, tilesetfile (incl terrain u16 data), local->global objtypes array, music_file
    maps.append((room,tileset,out_objects(map, mapfile)[1],music))

state_update = {}
for tsxfile in spritesheets :
    tsx = ET.parse(tsxfile).getroot()
    name = tsx.get('name')
    states = set(tsx.findall('./tile[@type]'))
    for state in states : 
        state_name = name+'_'+state.get('type')
        update  = get_property(state,'update','idle')
        state_update[state_name]= 'update_'+update

def export_tileset_collisions(tilesetname) : 
    TERRAINS = None,'grass','water','block','bump','fall','hit1'
    tsx = ET.parse(tilesetname).getroot()
    nbtiles   = int(tsx.get('tilecount'))
    ter_types = [ tt.get('name') for tt in tsx.findall('terraintypes/terrain') ]

    print ('const uint16_t tile_collisions[%d] = {'%(nbtiles+1), end='\n    ')
    for i in range(nbtiles+1) :
        tile = tsx.find('tile[@id="%d"]'%i)
        n=0
        if tile!=None :
            terrain = tile.get('terrain')
            if terrain != None : 
                t = [(ter_types[int(x)] if x else None) for x in terrain.split(',')]
                a,b,c,d = (TERRAINS.index(x) for x in t)
                n=a<<12 | b<<8 | c<<4 | d
        print ('0x%04x,'%n, end='\n    ' if i%8==7 else '')
    print ('};')

# --- output --------------------------------------------
print ('#ifndef DEFS_DECLARATION')
print ('#define DEFS_DECLARATION')

print ('#include "object.h"')
print ('#include "data.h"')
print ('#include "behaviours.h"')
print()

print ('''
struct RoomDef {
    const char *name;
    const uint16_t map;

    void (*enter)(uint8_t entry); // special case. often null
    const void *music;

    // local room types
    int nbtypes;
    ObjectType *types;
};
''')

print ("enum Rooms {")
for room in ROOMS :
    print ("    room_%s,"%(room))
print ("    room_NB")
print ('};\n')

print ('extern const struct RoomDef  room_defs[room_NB];')
print ('extern const uint16_t tile_collisions[];')

# TAR
print(f'''
#include "files.h"

#define X(name, block, sz) name, 
enum FilesEnum {{ NOFILE, FILE_LIST_XMACRO }};
#undef X

#define DATA_FILENAME "{DATA_FILENAME}"
extern const uint16_t data_files[][2]; // id, size
extern const char *data_filenames[];

enum class SendtoType {{ normal, fade, fall }};
struct SendToInfo {{
    uint16_t from, exit, to, entry;
    SendtoType type;
}};
extern const struct SendToInfo room_exits[{len(exits)}];
''')

print ('\n#endif // DEFS_DECLARATION\n')
print('\n// --- implementation --------------------------------------------\n')
print ("#ifdef DEFS_IMPLEMENTATION")

print()
print ('const struct RoomDef room_defs[room_NB] = {')
for room, tileset, types, music  in maps :
    print ('[room_%s]={'%room)    
    print ('\t.name="%s",'%room)
    print ('\t.map=%s_map,'%room)
    print ('\t.enter=%s ,'%0)
    print ('\t.music=%s,'%('data_%s_mod'%music if music else 0))
    print ('\t.nbtypes=%d,'%len(types))
    print ('\t.types=(ObjectType []){')
    for typ,stat in types :
        if stat==None : stat='???'
        print ("\t\t{ .file_id=%s_%s_spr, .update=%s},"%(typ,stat,state_update[typ+'_'+stat])) # ??? voluntarly not parsable
    print('    }},')
print('};')

export_tileset_collisions('rooms/tileset.tsx')
# TAR
print (f'''// -- files in .tar : id, sizes,  in order')
#define X(name,block,sz) {{ block, sz }},
const uint16_t data_files[][2] = {{ {{0,0}} , FILE_LIST_XMACRO }};
#undef X
#define X(name,block,sz) #name,
const char *data_filenames[] = {{ "nofile", FILE_LIST_XMACRO }};
#undef X
''')

print('const struct SendToInfo room_exits[] = {')
for start_room, start_exit_id, to_room, to_entry_id, ex_type in exits : 
    print (f'    {{ room_{start_room}, {start_exit_id}, room_{to_room}, {to_entry_id}, SendtoType::{ex_type} }},')
print('};')

print ("#endif // DEFS_IMPLEMENTATION")
# -- makefile
with open('build/sal.mk','w') as mkfile :
    tilesets = set(ts for _,ts,_,_ in maps)
    print ("ROOMS    :=",' '.join(ROOMS), file=mkfile)
    print ("TILESETS :=",' '.join(s.replace('.tsx','') for s in tilesets ), file=mkfile)
    print ("SPRITES  :=",' '.join(x[8:][:-4] for x in spritesheets ), file=mkfile)
    print ("STATES   :=",' '.join(state_update), file=mkfile)
    print ("MUSICS   :=",' '.join(musics), file=mkfile)
