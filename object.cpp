#include "object.h"
#include "filecache.h"

#include "game.h"
CollisionType max(CollisionType a, CollisionType b) {
	return static_cast<int>(a) > static_cast<int>(b) ? a : b;
}

void Object::init(const ObjectType &ot, uint8_t _value)
{
	message ("init object from file #%d\n", data_filenames[ot.file_id]);
	update=ot.update;

	value=_value;
	life = -1; // uninitialized yet
	state(ot.file_id);
	blitter_insert(this,0,1024,0);
}

void Object::destroy()
{
	blitter_remove(this);
	game.files.free_file(def());
	file_id=0;
	update=0;value=0;
}

void Object::state(const uint16_t _file_id)
{
	// unload previous state
	message("setting object from state %s to state %s\n", data_filenames[file_id], data_filenames[_file_id]);
	if (file_id) game.files.free_file(def());
	file_id=_file_id;
	sprite3_load(this,game.files.load_file(file_id)); 
	fr = 0;
}

bool Object::anim_frame()
{
	if (vga_frame%8==0) { // animate : 1 frame = 133ms
		// loop at end of animation.
		fr += 1;
		if (fr >= def()->frames) {
			fr=0;
			return true;
		}
	}
	return false;
}


// reject if bumps into blocking, depending on which part touches, or make it slide sideways
void Object::block (Collision col)
{
	if        (col.topleft  == CollisionType::block  && col.bottomleft  == CollisionType::block) {
		x+=1;
	} else if (col.topright == CollisionType::block  && col.bottomright == CollisionType::block) {
		x-=1;
	} else if (col.topleft  == CollisionType::block  && col.topright    == CollisionType::block) {
		y+=1;
	} else if (col.bottomleft == CollisionType::block&& col.bottomright == CollisionType::block) {
		y-=1;
	} else if (col.bottomleft == CollisionType::block) {
		y-=1; x+=1;
	} else if (col.bottomright == CollisionType::block) {
		y-=1; x-=1;
	} else if (col.topleft == CollisionType::block) {
		y+=1; x+=1;
	} else if (col.topright == CollisionType::block) {
		y+=1; x-=1;
	}
}

Collision Object::test_collide (const Object &other, CollisionType type ) const
{
	if (((x1() > other.x2()) || (x2() <  other.x1()) || (y2() <  other.y1()) || (y1() > other.y2())))
		return Collision(); // no collision

	// there is a collision. find where it is from object 
	// checks if ABCD. only 2 checks, others are done

	const int xm = (x1()+x2())/2;
	const int ym = (y1()+y2())/2;
	return Collision {
		.topleft    = other.x1()>xm || other.y1()>ym ? CollisionType::walk : type,
		.topright   = other.x2()<xm || other.y1()>ym ? CollisionType::walk : type,
		.bottomleft = other.x1()>xm || other.y2()<ym ? CollisionType::walk : type,
		.bottomright= other.x2()<xm || other.y2()<ym ? CollisionType::walk : type
	};
}