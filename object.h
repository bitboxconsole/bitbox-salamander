#pragma once

// On screen objects. Can be player, arm, portable objects or enemies.
#include "sdk/lib/blitter/blitter.h"
#include "miniz.h" // waitvsync

enum class CollisionType : uint8_t
{
	walk,
	grass,
	water,
	block, // player just blocked
	bump,  // player is rejected
	fall,
	hit1,  // rejected + invuln frames + gets hit points
	hit2,
};


CollisionType max(CollisionType a, CollisionType b);

struct Collision {
	CollisionType topleft, topright, bottomleft, bottomright;

	// priority update
	void operator |= (const Collision &c)
	{
		topleft     = max(topleft, c.topleft );
		topright    = max(topright, c.topright );
		bottomleft  = max(bottomleft, c.bottomleft );
		bottomright = max(bottomright, c.bottomright );
	} 
	void operator |= (const CollisionType ct) {
		topleft     = max(topleft, ct );
		topright    = max(topright, ct );
		bottomleft  = max(bottomleft, ct );
		bottomright = max(bottomright, ct );
	} 

	void print()
	{
		message ("collision : %d %d\n",topleft, topright );
		message ("            %d %d\n",bottomleft, bottomright );
	}

	explicit operator bool() const
	{	
		const auto w = CollisionType::walk;
		return topleft!=w || topright!=w || bottomleft!=w || bottomright!=w;
	}
};

struct Object;

struct ObjectType { // make it an virtual object ?
    const uint16_t file_id;
    void (*update)(Object *, Collision);
};

struct Object : public object {
	// an extended graphical object. always created in Room
	Object() : update(0)  {}

	void init(const ObjectType &ot, uint8_t _value); 
	void destroy();

	explicit operator bool() const { return this->update != 0; };

	void state (uint16_t file_id); // also set update ?
	uint16_t state () { return file_id; } // also set update ?

	// returns a pointer to header of sprite
	auto def() const { return reinterpret_cast<const SpriteFileHeader*>(a); }

	bool anim_frame(); // return true if anim just looped/finished

	void block (Collision collision); // updates object position to handle collisions

	// absolute hitbox. return a v4 ?
	int x1() const { return x + def()->x1; }
	int y1() const { return y + def()->y1; }
	int x2() const { return x + def()->x2; }
	int y2() const { return y + def()->y2; }
	
	Collision test_collide (const Object &other, CollisionType type = CollisionType::block) const;
	
	void hide() { d |=2;   }
	void show() { d &= ~2; }

	// additional members
	void (*update)(Object *, Collision); // to c ? delete flag to d ? virtual ? strategy pattern

	uint8_t value; // defined at load time
	int8_t life; // 0xff by default, will be defined by updates when first hit
	uint16_t file_id; // origin id of state
};
