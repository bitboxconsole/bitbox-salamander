<?xml version="1.0" encoding="UTF-8"?>
<tileset name="faces" tilewidth="16" tileheight="16" tilecount="56" columns="7">
 <image source="../../../../../../../bitbox-projects/salamander/rooms/faces_16x16.png" width="112" height="128"/>
 <tile id="1" type="pnj1"/>
 <tile id="14" type="father"/>
 <tile id="32" type="player"/>
 <tile id="37" type="guard_angry">
  <animation>
   <frame tileid="37" duration="100"/>
   <frame tileid="36" duration="100"/>
  </animation>
 </tile>
 <tile id="38" type="guard"/>
</tileset>
