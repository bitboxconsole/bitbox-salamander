#include "miniz.h"
#include "object.h"

// generics
void update_idle(Object *, Collision col); // do nothing
void update_playonce(Object *, Collision col); // play once and destroy object
void update_getit  (Object *, Collision col);
void update_sendto (Object *, Collision col); // send player to input 
void update_chest  (Object *, Collision col); // give object
void update_canpull  (Object *, Collision col); // can be pulled
void update_start_window(Object *, Collision col); // display/say something  if touched.

// rat
void update_rat_walk  (Object *, Collision col);
void update_rat_pause (Object *o, Collision col);
void update_rat_sleep (Object *, Collision col);

// start
void update_old_pnj(Object *self, Collision col);
void update_town_guard(Object *self, Collision col);

// forest
void update_dad(Object *self, Collision col);
void update_bee(Object *o, Collision col);
