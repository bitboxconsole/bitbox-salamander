#include "room.h"
#include "miniz.h" // resources
#include "sdk/lib/blitter/blitter.h"

extern "C" {
#include "sdk/lib/resources/tinymalloc.h" // t_available
}

#include "filecache.h"
#include "game.h"
#include "defs.h"

extern "C" {
#include "sdk/lib/mod/mod32.h"
void darken_mask(struct object *o, uint16_t mask);
void darken_fade(struct object *o, int v) ;
#define FADE_STEPS 5
}

#define ENTRY_OBJECT items16_entry_spr

// un/load all objects when entering a room

void Room::unload()
{
	// needed ? always keep them as static 
	blitter_remove(&background); 
	blitter_remove(&darken);
	
	// remove all objects. scan all objects, if valid, destroy
	for (int i=0;i<MAX_OBJECTS;i++) {
		if (!!objects[i])
			objects[i].destroy();
	}

	game.files.free_file(def->map);

	wait_vsync();	// let blitter flush old objects 
}

void Room::load(int room_id, int entry)
{
	// update status
	game.status.room = room_id;
	game.status.entry = entry;

	// here we only kept window, hero and sword - WHICH MUST BE IN FLASH ?
	def = &room_defs[room_id]; 
	message("Loading room %s, entry %d\n",def->name, entry);	
	load_tilemap(entry); 

	darken_fade(&darken, 0);
	blitter_insert(&darken,0,0,-6);

	if (def->enter)
		def->enter(entry);

	if (!SILENT && def->music)
		load_mod(def->music);
}

void Room::load_tilemap (const int entry)
{
	// fixme move elsewhere
	auto tset = reinterpret_cast<const struct TilesetFile*>(data_tileset_tset);
	if (tset->tilesize != 16) {
		message("tileset not 16x16 pixels\n");
		die(8,1);
	}

	if (tset->datacode != 1) { // u8 datacode
		message("tileset not 8bpp\n");
		die(8,2);
	}

	// --- load & verify tilemap 
	const struct TilemapFile *tmap = (struct TilemapFile *)game.files.load_file(def->map);
	if ( tmap->magic != 0xb17b ) { 
		message("Magic word not found on %s\n", def->map);
		die(8,4);
	}
	if (tmap->codec != 1) { // u8 !
		message("Bad encoding for tilemap ?\n");
		die(8,6);
	}

	message("Loading tilemap w: %d, h: %d, layers: %d\n", tmap->map_w, tmap->map_h, tmap->nb_layers);
	// fixme check no tid > tileset nb tiles ?

	tilemap_init ( &background, 
		tset->data, 0,0,
		TMAP_HEADER(tmap->map_w,tmap->map_h,TSET_16,TMAP_U8) | TSET_8bit, 
		tmap->data
	);
	this->tmap = (uint8_t *)tmap->data;

	blitter_insert(&background,0,0,128);	

	auto first = (const TilemapFileObject *) (tmap->data + tmap->map_w*tmap->map_h*tmap->nb_layers/2); // u8 encoding -> /2
	load_objects(first, entry );
}

// load objects, and position player at entry X
void Room::load_objects(const TilemapFileObject *first_object, const int entry )
{
	int z = 0; // first object group Z is 0, next are 100 (top layer)
	for (auto obj = first_object;!(obj->type == 0xff && obj->v[0] == 0xff); obj++) { // scan all object groups
		if (obj->type == 0xff) {
			z -= 100;
			continue;
		}
		if (obj->type > def->nbtypes) {
			message("Illegal type loaded from tilemap\n");
			die(7,4);
		}
		// entry : check if move player there
		if (def->types[obj->type].file_id == ENTRY_OBJECT) {
			if (entry==obj->v[0]) {
				message("player set to entry %d at: %d,%d\n",obj->v[0], obj->x, obj->y);
				player.x = obj->x;
				player.y = obj->y - player.h + 16;
			}
		} else {
			message(" + new object - type: %d %s val: %d at: %d,%d\n",
				obj->type, data_filenames[def->types[obj->type].file_id], obj->v[0], obj->x, obj->y);

			struct Object &o = new_object(def->types[obj->type],obj->v[0]);
			o.x = obj->x;
			o.y = obj->y;
			o.z = z++;
		}
	}
	bg_scroll();
}

void Room::fadeIn()
{
	message("fadein\n");
	for (int i=1;i<FADE_STEPS;i++) {
		darken_fade(&darken,i);
		wait_vsync(6);
	}
}
void Room::fadeOut()
{
	message("fadeout\n");
	for (int i=FADE_STEPS;i>0;i--) {
		darken_fade(&darken,i);
		wait_vsync(6);
	}
}

// if possible put player in center of bg by moving room & all entities
void Room::bg_scroll(void)
{
	// move viewport to entry position so that player is at center

	// next position of the background so that it's centered
	int nx = background.x +VGA_H_PIXELS/2-player.x; 
	int ny = background.y +VGA_V_PIXELS/2-player.y;

	// make sure the background will not touch level borders
	if (nx>0) nx=0;
	if (ny>0) ny=0;
	if (nx+background.w < VGA_H_PIXELS)	nx=VGA_H_PIXELS-background.w;
	if (ny+background.h < VGA_V_PIXELS)	ny=VGA_V_PIXELS-background.h;	

	// 	compute movement from current position
	int dx = nx - background.x;
	int dy = ny - background.y;

	if (dx==0 && dy==0) return;
	
	// move backgound to new place
	background.x += dx;
	background.y += dy;

	// move every objects relative to the background
	for (auto &obj : objects) {
		if (obj) {
			obj.x += dx;
			obj.y += dy;
		}
	}

	// also move player
	player.x += dx;
	player.y += dy;
}


CollisionType Room::bg_property_at(int x, int y) const
{
	uint8_t tileid = tmap[(y-background.y)/16*background.w/16+(x-background.x)/16];
	// 4 corners in a tile
	uint16_t terrains_tile = tile_collisions[tileid-1]; // 1-based (empty)
	if (x%16<8) { 	// left
		return static_cast<CollisionType>(y%16<8 ? (terrains_tile>>12)&0xf : (terrains_tile>>4)&0xf);
	} else { 		// right
		return static_cast<CollisionType>(y%16<8 ? (terrains_tile>> 8)&0xf : (terrains_tile>>0)&0xf);
	}
}


Collision Room::bg_collide(const Object &object) const // object collision state for each corner of the sprite
{
	const int bx1=object.x1();
	const int by1=object.y1();
	const int bx2=object.x2();
	const int by2=object.y2();

	return Collision {
		.topleft    = bg_property_at (bx1,by1),
		.topright   = bg_property_at (bx2,by1),
		.bottomleft = bg_property_at (bx1,by2),
		.bottomright= bg_property_at (bx2,by2)
	};
}

void Room::frame()	// -- Update positions / animations
{

	auto col = bg_collide(player);
	// check all objects for collisions (+sword / projectile)
	// mark object value to collided / hit ? 
	for (auto &obj : objects) {
		if (obj) { // fixme and object can_collide ?
			auto objcol = player.test_collide(obj);
			if (objcol) {
				col |= objcol;
			}
			obj.update(&obj, objcol);
			obj.anim_frame();
		}
	}

	player.update(&player, col);
	player.invincible(); // blink if invicible - for all update callbacks
	bg_scroll();

	// flashes / night, FIXME: add borders, night only, ...
	if (game.status.night) {
		if ((vga_frame/8)%60)
			darken_mask(&darken,0b11111110*0x101); // amber/night
		else
			darken_mask(&darken,0xffff); // flashes
	} else {
			darken_mask(&darken,0xffff); // flashes
	}
}

Object & Room::new_object(const ObjectType &objtype, uint8_t _value)
{
	for (int i=0;i<MAX_OBJECTS;i++) {
		Object &o=objects[i];
		if (!o) {
			o.init(objtype,_value);
			return o;
		}
	}
	message("No more free slots for objects available !\n");
	die(8,8);
}
