struct Gamepad 
{
	static bool up()   { return GAMEPAD_PRESSED(0,up)   || gamepad_y[0] < -50; }
	static bool down() { return GAMEPAD_PRESSED(0,down) || gamepad_y[0] > 50;  }

	static bool left() { return GAMEPAD_PRESSED(0,left) || gamepad_x[0] < -50; }
	static bool right(){ return GAMEPAD_PRESSED(0,right)|| gamepad_x[0] > 50;  }

	static Dir dir() {
		if (up())    return Dir::up;
		if (down())  return Dir::down;
		if (left())  return Dir::left;
		if (right()) return Dir::right;
		return Dir::none;
	}

	static bool attack() 
	{
		return GAMEPAD_PRESSED(0,A); 
	}

	// wait for new joystick button pressed, send it.
	static int wait_pressed() 
	{
		// wait for release first 
		while (gamepad_buttons[0]);

		uint16_t prev_buttons=gamepad_buttons[0];
		while (1) {
			/* only if analog gamepad used
			if ( gamepad_y[0] < -50 ) gamepad_buttons[0] |= gamepad_up;
			if ( gamepad_y[0] >  50 ) gamepad_buttons[0] |= gamepad_down;
			if ( gamepad_x[0] < -50 ) gamepad_buttons[0] |= gamepad_left;
			if ( gamepad_x[0] >  50 ) gamepad_buttons[0] |= gamepad_right;
			*/
			uint16_t but = gamepad_buttons[0];
			uint16_t pressed = (but & ~prev_buttons); // 1+lsb pos or zero

			if (pressed) 
				return pressed;

			prev_buttons = but;
			wait_vsync(1);
		}
	}
};
