// a special "object" that just darkens what's below (by clearing bits)
// useful for fade in/outs
// for now just darkens the whole line
// layout : a = u16 mask, allows couple patterns, swap pixels each line

// FIXME don't assume 8bits ...
#include "sdk/lib/blitter/blitter.h"

// TODO make a fade pattern corresponding to a level of shade 0-16 
/*
for int(i=1; i<8;i++) pattern(i,i-1) pattern(i,i)
a = i<<5 | i&6 << 3 | i
b : idem i ou i-1
precalc 16 patterns a l'init

fade(0-16)
*/

static void skip(object *o) {}

static void darken_line_8_mask (struct object *o)
{
	uint32_t mask=o->a;
	// maybe swap bytes (pixels)
	if (vga_line%2)
		mask = (mask&0xff00ff)<<8 | (mask&0xff00ff00)>>8;

	uint32_t *src = (uint32_t *)draw_buffer;
	for (int i=0;i<VGA_H_PIXELS/4;i++) {
		src[i] &= mask;
	}
}

static void half_line(struct object *o)
{
	uint32_t *src = (uint32_t *)draw_buffer;
	for (int i=0;i<VGA_H_PIXELS/4;i++) { 
		src[i]=(src[i]&(0b11010110UL*0x01010101))>>1;
	}
}

static void quarter_line(struct object *o)
{
	uint32_t *src = (uint32_t *)draw_buffer;
	for (int i=0;i<VGA_H_PIXELS/4;i++) {
		src[i]=(src[i]&(0b10000100UL*0x01010101))>>2; // divide by 4, 32bits
	}
}

//--- init functions
// prepare object. also use it for changing values

// nice effects. alternate pixels every line if different
void darken_mask(struct object *o, uint16_t mask) 
{
	o->w=VGA_H_PIXELS;
	o->h=VGA_V_PIXELS;
	o->frame=0;

	o->a = (uint32_t)mask*0x10001; // 16 -> 32 bits
	o->line = mask==0xffff ? skip : darken_line_8_mask ;
}

// value 0-4 for now
void darken_fade(struct object *o, int value)
{
	o->w=VGA_H_PIXELS;
	o->h=VGA_V_PIXELS;
	o->frame=0;
	switch(value)
	{
		case 0: darken_mask(o,0x0000);  break; // hide all
		case 1: o->line = quarter_line; break;
		case 2: o->line = half_line;    break;
		case 3: darken_mask(o,0b10010100*0x0101); break; // three quarters
		default: 
			o->line = skip;
			break; // full : do nothing
	}
	// alternate lines ...
}
