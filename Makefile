
# must be empty for english
#TRANSLATE=_fr

NAME:=salamander$(TRANSLATE)

GAME_C_FILES = game.cpp \
	sdk/lib/blitter/blitter.c \
	sdk/lib/blitter/blitter_tmap.c \
	sdk/lib/blitter/blitter_sprites3.c \
	sdk/lib/blitter/blitter_surface.c \
	sdk/lib/mod/mod32.c\
	blit_darken.c \
	object.cpp \
	player.cpp \
	room.cpp \
	behaviours.cpp \
	window.cpp \
	room_start.cpp \
	room_forest.cpp \
	instances.c
	#resources.c \
#$(ROOMS:%=room_%$(TRANSLATE).c) \

DEFINES = VGA_MODE=320 VGA_BPP=8
USE_SDCARD:=yes
NO_USB:=yes

include sdk/kernel/bitbox.mk
# defines ROOMS, SPRITES, STATES, MUSICS	

# generates all dependencies of data
.DELETE_ON_ERROR: build/data.mk defs.h files.h
build/sal.mk defs.h: mk_defs.py rooms/tileset.tsx $(ROOMS:%=rooms/%.tmx)
	@mkdir -p build
	@mkdir -p data
	python3 mk_defs.py > defs.h

-include build/sal.mk

#$(TILESETS:%=data/%.tset)
DATAFILES  := $(MUSICS:%=sound/%.mod) \
	data/window_bg.spr \
	data/message_bg.spr \
	data/talk_bg.spr \
	data/window_hud.tset \
	data/mini.fon \
	data/tileset.tset

SDFILES := $(sort $(ROOMS:%=data/%.map) $(STATES:%=data/%.spr))

# Prepare window data
data/window.tset: rooms/window.png
	sdk/lib/blitter/scripts/mk_tset.py -s 8 -p MICRO rooms/window.png
	mv rooms/window.tset data/

data/%.spr: rooms/%.png
	sdk/lib/blitter/scripts/mk_spr.py $^ -o $@ -p MICRO

data/window_hud.tset: rooms/window_hud_ts.png
	sdk/lib/blitter/scripts/mk_tset.py -s 8 -p MICRO $^
	mv rooms/window_hud_ts.tset $@

data/mini.fon: font_mini.png
	sdk/lib/blitter/scripts/mk_font.py $^ 
	mv font_mini.fon $@

game.cpp: defs.h data.h files.h
$(STATES:%=data/%.spr): $(SPRITES:%=build/sprite_%)

data.h : $(DATAFILES)
	sdk/lib/resources/embed.py $^ > data.h

$(NAME).tar : $(SDFILES)
	tar -cf $(NAME).tar $(SDFILES)

files.h: tar2h.py salamander.tar 
	python3 tar2h.py salamander.tar > $@

%_fr.c: %.c trans_fr.po
	./i18n.py $< fr

data/%.spr: build/

build/sprite_%: sprites/%.tsx
	sdk/lib/blitter/scripts/mk_spr.py $^ -o data -p MICRO > $@

data/%.map: rooms/%.tmx
	sdk/lib/blitter/scripts/mk_tmap.py $^ #> /dev/null # ignore output .h file
	mv $*.map data

data/%.tset: rooms/%.tsx
	sdk/lib/blitter/scripts/mk_tset.py $^ -p MICRO
	mv rooms/$*.tset data/

clean_data:
	rm -rf build/sprite* defs.h data/* data.h room_*_fr.c files.h defs.h
